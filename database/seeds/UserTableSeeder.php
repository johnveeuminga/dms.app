<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'firstname' => 'Admin',
        	'middlename' => 'Admin',
        	'lastname' => 'Admin',
        	'username' => 'admin',
        	'password' => bcrypt('password'),
            'usertype_id' => 1
        ]);

         DB::table('users')->insert([
            'firstname' => 'John Vee',
            'middlename' => 'L',
            'lastname' => 'Uminga',
            'username' => '13-2320-240',
            'password' => bcrypt('password'), 
            'usertype_id' => 2
        ]);
    }
}
