<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() 
    {
        $this->call(OrderTypeTablesSeeder::class);
        $this->call(UsertypesTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(MembersTableSeeder::class);
    }   
}
