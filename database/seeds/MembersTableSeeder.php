<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members')->insert([
        	'firstname' => 'John Vee',
        	'middle_name' => 'Labfey',
        	'lastname' => 'Uminga',
        	'rank' => 'P2LT',
        	'afspn' => null,
        	'unit_assignment' => 'test',
        	'designation' => 'test',
        ]);
    }
}
