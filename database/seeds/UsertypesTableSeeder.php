<?php

use Illuminate\Database\Seeder;

class UsertypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usertypes')->insert([
        	'usertype_desc' => 'Administrator',
        	'usertype_code' => 'admin',
        ]);

        DB::table('usertypes')->insert([
        	'usertype_desc' => 'User',
        	'usertype_code' => 'user',
        ]);

        DB::table('usertypes')->insert([
            'usertype_desc' => 'Guest',
            'usertype_code' => 'guest',
        ]);
    }
}
