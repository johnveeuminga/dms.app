<?php

use Illuminate\Database\Seeder;

class OrderTypeTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_types')->insert([
        	'ordertype_desc' => 'Appointment Order',
        	'ordertype_code' => 'APPOR',
        ]);

        DB::table('order_types')->insert([
        	'ordertype_desc' => 'Resignation Order',
        	'ordertype_code' => 'RESOR',
        ]);

        DB::table('order_types')->insert([
        	'ordertype_desc' => 'Retirement Order',
        	'ordertype_code' => 'RETOR',
        ]);

        DB::table('order_types')->insert([
            'ordertype_desc' => 'Order To Duty',
            'ordertype_code' => 'ORDTODUT',
        ]);

        DB::table('order_types')->insert([
            'ordertype_desc' => 'Promotion Order',
            'ordertype_code' => 'PROMOR',
        ]);

        DB::table('order_types')->insert([
            'ordertype_desc' => 'Extension of Appointment',
            'ordertype_code' => 'EXTAPP',
        ]);

        DB::table('order_types')->insert([
            'ordertype_desc' => 'Others',
            'ordertype_code' => 'OTHER',
        ]);
    }
}
