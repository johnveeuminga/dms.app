<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_tors', function (Blueprint $table) {
           $table->increments('id');
            $table->timestamps();
            $table->string('path');
            $table->string('filename')->nullable();
            $table->integer('member_id')->unsigned();

            $table->foreign('member_id')
                  ->references('id')
                  ->on('members')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_tors');
    }
}
