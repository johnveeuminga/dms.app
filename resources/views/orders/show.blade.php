@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel">
					<div class="panel-heading"><strong>View Sales Order</strong></div>
					<div class="panel-body">
						 @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <strong>{{session()->get('success')}}</strong> 
                            </div>
                        @endif
						<div class="row">
							<div class="col-md-3 col-md-offset-2"><strong>Filename: </strong></div>
							<div class="col-md-7">{{$order->filename}}</div>
						</div>
						<div class="row">
							<div class="col-md-3 col-md-offset-2"><strong>Order Type: </strong></div>
							<div class="col-md-7">{{$order->ordertype->ordertype_desc}}</div>
						</div>
						<div class="row">
							<div class="col-md-3 col-md-offset-2"><strong>Members Tagged: </strong></div>
							<div class="col-md-7">
								 @foreach($order->members as $member)
                                    <p>
                                        <a href='{{url("members/$member->id")}}'>{{$member->firstname}} {{$member->middlename}} {{$member->lastname}}</a>
                                    </p>
                                @endforeach 
                             </div>
						</div>
						<div class="row">
							<div class="col-md-3 col-md-offset-2"><strong>Description: </strong></div>
							<div class="col-md-5">
								<textarea class="form-control" readonly="true">{{$order->description}}</textarea>
							</div>
						</div>
						<hr>
						<div class="btn-container pull-right">
							<a class="btn btn-info" href='{{url("/orders/$order->id/download")}}' role="button">Download File</a>
							@if(Auth::user()->usertype_id!=3)
								<a class="btn btn-primary" href='{{url("/orders/$order->id/edit")}}' role="button">Edit</a>
								<a class="btn btn-danger" href='#delete-modal' data-toggle="modal">Delete</a>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="delete-modal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Confirm Delete</h4>
				</div>
				<div class="modal-body">
					<p style="font-size: 18px">You are about to delete a record. This procedure is irreversible.</p>
					<p style="font-size: 18px">Do you want to proceed?</p>
					<form id="delete-form" method="POST" action='{{url("/orders/$order->id")}}'>
						{{csrf_field()}}
						{{method_field('DELETE')}}
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<input type="submit" form="delete-form" class="btn btn-danger" value="Delete"></input>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
@endsection