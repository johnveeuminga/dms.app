@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><strong>Order Management</strong></div>
					<div class="panel-body">
						@if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <strong>{{session()->get('success')}}</strong> 
                            </div>
                        @endif
{{--                         <div class="col-md-4">
                             <select id="search">
                                <option value="">Search for a user.</option>
                                @foreach($orders as $order)
                                    <option value="{{$order->id}}">{{$member->firstname}} {{$member->middle_initial}}. {{$member->lastname}}</option>
                                @endforeach
                            </select>
                        </div> --}}
                        <table class="table" id="order-table">
                        	<thead>
                        		<tr>
                        			<th>Document</th>
                        			<th>Ordertype</th>
                        			<th>Description</th>
                        			<th>Members Tagged</th>
                        			<th></th>
                        		</tr>
                        	</thead>
                        	<tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <th>{{$order->filename}}</th>
                                        <td>{{$order->ordertype->ordertype_desc}}</td>
                                        <td>{{$order->description}}</td>
                                        <td>
                                        @foreach($order->members as $member)
                                            <p>
                                                <a href='{{url("members/$member->id")}}'>{{$member->firstname}} {{$member->middlename}} {{$member->lastname}}</a>
                                            </p>
                                        @endforeach 
                                        </td>
                                        <td><a class="btn btn-primary" href='{{url("/orders/$order->id")}}}' role="button">View</a></td>
                                    </tr>
                                @endforeach
                        	</tbody>
                        </table>
                        @if(Auth::user()->usertype_id!=3)
                             <a class="btn btn-primary" href='{{url("orders/create")}}' role="button">Add</a>
                        @endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script>
        $('#order-table').DataTable();
    </script>
@endsection