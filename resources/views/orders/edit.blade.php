@extends('layouts.app')

@section('content')
	<div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Edit Order</strong></div>
                    <div class="panel-body">
                        <h3 style="margin-bottom: 40px;" class="text-center">Enter New Order Information</h3>
                        <form action='{{url("/orders/$order->id")}}' method="POST" files="true" enctype="multipart/form-data"> 
                            {{csrf_field()}}
                            <div class="form-group row{{ $errors->has('orderFile') ? ' has-error' : '' }}">
                                <label for="order-file" class="col-sm-3 col-md-offset-1 form-control-label">Document:</label>
                                <div class="col-sm-7">
                                    <input type="file" accept=".docx, .doc, .pdf, .jpg, .jpeg" class="form-control" id="order-file" name="orderFile">

                                    @if ($errors->has('orderFile'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('orderFile') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>

                            <div class="form-group row{{ $errors->has('ordertype') ? ' has-error' : '' }}">
                                <label for="order-type" class="col-sm-3 col-md-offset-1 form-control-label">Document Type:</label>
                                <div class="col-sm-7">
                                    <select name="ordertype" class="form-control" id="order-type">
                                        @foreach($ordertypes as $ordertype)
                                            <option value="{{$ordertype->id}}" {{ (old("ordertype") == $ordertype->id ? "selected": $order->ordertype_id == $ordertype->id ? "selected" : "") }}>{{$ordertype->ordertype_desc}}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('ordertype'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ordertype') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('member') ? ' has-error' : '' }}">
                                <label for="tag-member" class="col-sm-3 col-md-offset-1 form-control-label">Members:</label>
                                <div class="col-sm-7">
                                    <select id="tag-member" name="member[]" multiple >
                                        @foreach($members as $member)
                                            <option value="">Search for a member.</option>
                                            <option value="{{$member->id}}" {{$order->members->contains('id', $member->id)?"selected":""}}>{{$member->firstname}} {{$member->middle_name}} {{$member->lastname}}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('member'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('member') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('comment') ? ' has-error' : '' }}">
                                <label for="comment" class="col-sm-3 col-md-offset-1 form-control-label">Description:</label>
                                <div class="col-sm-7">
                                     <textarea class="form-control" rows="4" id="comment" name="comment">{{$errors->has('comment') ? old('comment'):  $order->description }}</textarea>
                                     @if ($errors->has('comment'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('comment') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-4">
                                    <button type="submit" class="btn btn-secondary" >Submit</button>
                                </div>
                            </div>
                            <input type="hidden" name="fileName" value="order-{{$member->lastname}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
	</div>	
@endsection

@section('scripts')
    <script>
        $('#tag-member').selectize({
            hightlight: false,
            placeholder: 'Tag a member'
        });
    </script>
@endsection