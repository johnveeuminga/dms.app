@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong>Change Password</strong>
					</div>
					<div class="panel-body">
						<form action="{{url('/profile/changepass')}}" method='POST'>
							{{method_field('PUT')}}
							{{csrf_field()}}

							@if(session()->has('oldpass'))
								<div class="alert alert-danger alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										<span class="sr-only">Close</span>
									</button>
									<strong>{{session()->get('oldpass')}}</strong> 
								</div>
							@endif

							@if(session()->has('success'))
								<div class="alert alert-success alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										<span class="sr-only">Close</span>
									</button>
									<strong>{{session()->get('success')}}</strong> 
								</div>
							@endif

							<div class="form-group row {{ $errors->has('oldpass') ? ' has-error' : '' }}">
                        		<label for="oldpass" class="col-sm-3 col-md-offset-1 form-control-label">Old Password</label>
                        		<div class="col-sm-6">
                        			<input type="password" class="form-control" id="oldpass" name="oldpass" placeholder="Old Password" value="">

                        			@if ($errors->has('oldpass'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('oldpass') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class="form-group row {{ $errors->has('newpass') ? ' has-error' : '' }}">
                        		<label for="newpass" class="col-sm-3 col-md-offset-1 form-control-label">New Password</label>
                        		<div class="col-sm-6">
                        			<input type="password" class="form-control" id="newpass" name="newpass" placeholder="New Password" value="">

                        			@if ($errors->has('newpass'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('newpass') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class="form-group row {{ $errors->has('newpass_confirmation') ? ' has-error' : '' }}">
                        		<label for="newpass_confirmation" class="col-sm-3 col-md-offset-1 form-control-label">Confirm Password</label>
                        		<div class="col-sm-6">
                        			<input type="password" class="form-control" id="newpass_confirmation" name="newpass_confirmation" placeholder="Confirm Password" value="">

                        			@if ($errors->has('newpass_confirmation'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('newpass_confirmation') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class="form-group row">
                        		<div class="col-sm-10 col-sm-offset-4">
                        			<button type="submit" class="btn btn-primary">Save</button>
                        		</div>
                        	</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection