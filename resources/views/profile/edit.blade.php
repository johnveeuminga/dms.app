@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong>Edit Profile</strong>
					</div>

					<div class="panel-body">
						 @if(session()->has('success'))
							<div class="alert alert-success alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<strong>{{session()->get('success')}}</strong> 
							</div>
						@endif

                        <h3 style="margin-bottom: 40px;" class="text-center">Profile Information</h3>

						<form action="{{url('/profile')}}" method="POST">
							{{method_field('PUT')}}
							{{csrf_field()}}
							<div class="form-group row {{ $errors->has('firstname') ? ' has-error' : '' }}">
                        		<label for="firstname" class="col-sm-2 col-md-offset-1 form-control-label">Firstname</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname" value="{{$errors->has('firstname') ? old('firstname'):  Auth::user()->firstname }}">

                        			@if ($errors->has('firstname'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('firstname') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class="form-group row {{ $errors->has('middlename') ? ' has-error' : '' }}">
                        		<label for="middlename" class="col-sm-2 col-md-offset-1 form-control-label">Middlename</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="middlename" name="middlename" placeholder="Middlename" value="{{$errors->has('middlename') ? old('middlename'):  Auth::user()->middlename }}">

                        			@if ($errors->has('middlename'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('middlename') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class="form-group row {{ $errors->has('lastname') ? ' has-error' : '' }}">
                        		<label for="lastname" class="col-sm-2 col-md-offset-1 form-control-label">Lastname</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="lastname" name="lastname" placeholder="lastname" value="{{$errors->has('lastname') ? old('lastname'):  Auth::user()->lastname }}">

                        			@if ($errors->has('lastname'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('lastname') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class="form-group row">
                        		<div class="col-sm-10 col-sm-offset-3">
                        			<button type="submit" class="btn btn-primary">Save</button>
                        		</div>
                        	</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection