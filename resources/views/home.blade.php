@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <p>Welcome {{Auth::user()->firstname}}!</p>
                    <p>Please select a task below: </p>
                    <ul>
                        @if(Auth::user()->usertype_id == 1)
                            <li><a href="{{url('/users')}}">User Management</a></li>
                        @endif
                        <li><a href="{{url('/members')}}">COP Profile Management</a></li>
                        <li><a href="{{url('/orders')}}">Document Repository</a></li>
                    </ul>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
