@if(!isset($members))
	<tr>
		<th>{{$member->firstname}} {{$member->middle_initial}}. {{$member->lastname}}</th>
	    <td>{{$member->rank}}</td>
	    <td>
	    	@if($member->afspn)
	    		{{$member->afspn}}
	    	@else
	    		Empty
	    	@endif
	    </td>
	    <td>{{$member->unit_assignment}}</td>
	    <td>{{$member->designation}}</td>
	    <td><a class="btn btn-primary" href='{{url("members/$member->id")}}' role="button">View</a></td>
	</tr>
@else
	@foreach($members as $member)
        <tr>
            <th>{{$member->firstname}} {{$member->middle_initial}}. {{$member->lastname}}</th>
            <td>{{$member->rank}}</td>
            <td>
            	@if($member->afspn)
            		{{$member->afspn}}
            	@else
            		Empty
            	@endif
            </td>
            <td>{{$member->unit_assignment}}</td>
            <td>{{$member->designation}}</td>
            <td><a class="btn btn-primary" href='{{url("members/$member->id")}}' role="button">View</a></td>
        </tr>
    @endforeach
@endif