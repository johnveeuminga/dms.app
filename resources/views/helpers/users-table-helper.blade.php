@if(isset($user))
	<tr>
	    <th>{{$user->employee_number}}</th>
	    <td>{{$user->firstname}}</td>
	    <td>{{$user->lastname}}</td>
	    <td>{{$user->usertype->usertype_desc}}</td>
	    <td class="text-center">
	        <a class="btn btn-primary" href='{{url("users/$user->id/edit")}}' role="button">Edit</a>

	        <a class="btn btn-danger" data-form-url = '{{url("users/$user->id")}}' data-toggle="modal" href="#modal-delete">Delete</a>
	    </td>
	</tr>
@else
	@foreach($users as $user)
        <tr>
	        <th>{{$user->employee_number}}</th>
	        <td>{{$user->firstname}}</td>
	        <td>{{$user->lastname}}</td>
	        <td>{{$user->usertype->usertype_desc}}</td>
	        <td class="text-center">
	            <a class="btn btn-primary" href='{{url("users/$user->id/edit")}}' role="button">Edit</a>

	            <a class="btn btn-danger" data-form-url = '{{url("users/$user->id")}}' data-toggle="modal" href="#modal-delete">Delete</a>
	        </td>
	    </tr>
    @endforeach
@endif