@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>User Management</strong></div>
                    <div class="panel-body">
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <strong>{{session()->get('success')}}</strong> 
                            </div>
                        @endif
                        <table class="table" id="users-table">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Firstname</th>
                                    <th>Lastname</th>
                                    <th>Usertype</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <th>{{$user->username}}</th>
                                    <td>{{$user->firstname}}</td>
                                    <td>{{$user->lastname}}</td>
                                    <td>{{$user->usertype->usertype_desc}}</td>
                                    <td class="text-center">
                                        <a class="btn btn-primary" href='{{url("users/$user->id/edit")}}' role="button">Edit</a>

                                        <a class="btn btn-danger" data-form-url = '{{url("users/$user->id")}}' data-toggle="modal" href="#modal-delete">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <a class="btn btn-primary" href="{{url('users/create')}}" role="button">Add</a>
                    </div>
                </div>
            </div>
        </div>
	</div>

    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Delete User</h4>
                </div>
                <div class="modal-body">
                    <p>You are about to delete a user. This process is irreversible.</p>
                    <p>Do you want to proceed?</p>
                    <form action="#" method="POST" id="delete-form">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-danger" value="Delete" form="delete-form">
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('scripts')
    <script>
        $('#modal-delete').on('show.bs.modal', function (event){
            var trigger = $(event.relatedTarget);
            var formUrl = trigger.data('form-url');

            var modal = $(this);

            modal.find('#delete-form').attr('action', formUrl);

        });
    </script>
    <script>
        // $('#search').selectize({
        //     highlight:false,
        //     placeholder: "Enter a user's name",
        //     allowEmptyOptions: true,
        //     onChange : function(value){
        //         $('#members-tbody').html('<h2 class="text-center">Please wait....</h2>').show();
        //         var url;
        //         if(value!=""){
        //             url= "{{url("/users/")}}"+"/"+value+"/get";

        //         }else{
        //             url= "{{url("/users/")}}"+"/all"+"/getAll";
        //         }

        //         $.ajax({
        //             url: url,
        //             method: 'GET',
        //             header: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        //             dataType: 'html',
        //             success: function(response){
        //                 $('#users-table tbody').html(response).show();
        //             }
        //         })
        //     }


        // });

        $('#users-table').DataTable();
    </script>
@endsection