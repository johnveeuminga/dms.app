@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Edit User</strong></div>

                    <div class="panel-body">
                        <h3 style="margin-bottom: 40px;" class="text-center">Edit User Information</h3>
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <form action='{{url("users/$user->id")}}' method="POST">
                            {{method_field('PUT')}}
                        	{{csrf_field()}}
                            <input type="hidden" name="id" value="{{$user->id}}">
                        	<div class="form-group row {{ $errors->has('firstname') ? ' has-error' : '' }}">
                        		<label for="firstname" class="col-sm-3 col-md-offset-1 form-control-label">Firstname</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname" value="{{$errors->has('firstname') ? old('firstname'):  $user->firstname }}">

                        			@if ($errors->has('firstname'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('firstname') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>
                        	<div class="form-group row {{ $errors->has('middlename') ? ' has-error' : '' }}">
                        		<label for="middle_initial" class="col-sm-3 col-md-offset-1 form-control-label">Middle Initial</label>
                        		<div class="col-sm-3">
                        			<input type="text" class="form-control" id="middle_initial" name="middle_initial" placeholder="Middle Initial" value="{{$errors->has('middlename') ? old('middlename'):  $user->middlename }}">
                        			@if ($errors->has('middlename'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('middlename') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>
                        	<div class=" form-group row {{ $errors->has('lastname') ? ' has-error' : '' }}">
                        		<label for="lastname" class="col-sm-3 col-md-offset-1  form-control-label">Lastname</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Lastname" value="{{$errors->has('lastname') ? old('lastname'):  $user->lastname }}">
                        			@if ($errors->has('lastname'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('lastname') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class="form-group row {{ $errors->has('username') ? ' has-error' : '' }}">
                        		<label for="username" class="col-sm-3 col-md-offset-1 form-control-label">Username</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="username" name="username" placeholder="Employee Number" value="{{$errors->has('username') ? old('username'):  $user->username }}">

                        			@if ($errors->has('username'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('username') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class="form-group row">
		      					<label for="Usertype" class="col-sm-3 col-md-offset-1  form-control-label">Usertype</label>
		      					<div class="col-sm-7">
		      						<select name="usertype_id" id="usertype" class="form-control">
		      							@foreach($usertypes as $usertype)
		      								<option value="{{$usertype->id}}" {{ (old("usertype_id") == $usertype->id ? "selected": $user->usertype_id == $usertype->id ? "selected" : "") }}>{{$usertype->usertype_desc}}</option>
		      							@endforeach
		      						</select>
		      					</div>
		      				</div>
                        	
                        	<div class="form-group row">
                        		<div class="col-sm-10 col-sm-offset-3">
                        			<button type="submit" class="btn btn-secondary">Save</button>
                        		</div>
                        	</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	</div>
@endsection