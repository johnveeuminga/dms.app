@extends('layouts.app')

@section('content')
	<div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Add User</strong></div>

                    <div class="panel-body">
                        <h3 style="margin-bottom: 40px;" class="text-center">Enter User Information</h3>
                        <form action="{{url('users')}}" method="POST">
                        	{{csrf_field()}}
                        	<div class="form-group row {{ $errors->has('firstname') ? ' has-error' : '' }}">
                        		<label for="firstname" class="col-sm-3 col-md-offset-1 form-control-label">Firstname</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname" value="{{ old('firstname') }}">

                        			@if ($errors->has('firstname'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('firstname') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>
                        	<div class="form-group row {{ $errors->has('middlename') ? ' has-error' : '' }}">
                        		<label for="middlename" class="col-sm-3 col-md-offset-1 form-control-label">Middle Initial</label>
                        		<div class="col-sm-3">
                        			<input type="text" class="form-control" id="middlename" name="middlename" placeholder="Middle Initial" value="{{ old('middlename') }}">
                        			@if ($errors->has('middlename'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('middlename') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>
                        	<div class=" form-group row {{ $errors->has('lastname') ? ' has-error' : '' }}">
                        		<label for="lastname" class="col-sm-3 col-md-offset-1  form-control-label">Lastname</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Lastname" value="{{ old('lastname') }}">
                        			@if ($errors->has('lastname'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('lastname') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>
                        	<div class="form-group row {{ $errors->has('username') ? ' has-error' : '' }}">
                        		<label for="username" class="col-sm-3 col-md-offset-1 form-control-label">Username</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="username" name="username" placeholder="username" value="{{ old('username') }}">

                        			@if ($errors->has('username'))
      	                                    <span class="help-block">
      	                                        <strong>{{ $errors->first('username') }}</strong>
      	                                    </span>
      	                                @endif
                        		</div>
                        	</div>

                        	<div class="form-group row">
      					<label for="Usertype" class="col-sm-3 col-md-offset-1  form-control-label">Usertype</label>
      					<div class="col-sm-7">
      						<select name="usertype_id" id="usertype" class="form-control">
      							@foreach($usertypes as $usertype)
      								<option value="{{$usertype->id}}" {{ (old("usertype_id") == $usertype->id ? "selected":"") }}>{{$usertype->usertype_desc}}</option>
      							@endforeach
      						</select>
      					</div>
      				</div>

                        	<div class=" form-group row {{ $errors->has('password') ? ' has-error' : '' }}">
                        		<label for="password" class="col-sm-3 col-md-offset-1  form-control-label">Password</label>
                        		<div class="col-sm-7">
                        			<input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        			@if ($errors->has('password'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('password') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class=" form-group row">
                        		<label for="password-confirm" class="col-sm-3 col-md-offset-1  form-control-label">Confirm Password</label>
                        		<div class="col-sm-7">
                        			<input type="password" class="form-control" id="password-confirm"  name="password_confirmation" placeholder="Confirm Password">
                        		</div>
                        	</div>
                        	
                        	<div class="form-group row">
                        		<div class="col-sm-10 col-sm-offset-4">
                        			<button type="submit" class="btn btn-secondary">Save</button>
                        		</div>
                        	</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	</div>	
@endsection