@extends('layouts.app')

@section('content')
	<div class="container show-member">
		<div class="row">
			<div class="col-md-8 col-md-offset-2" style="float: none">
				 <div class="panel panel-default">
				 	<div class="panel-body">
					 	@if(session()->has('success'))
							<div class="alert alert-success alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<strong>{{session()->get('success')}}</strong> 
							</div>
						@endif

						@if(session()->has('file_success'))
							<div class="alert alert-success alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<strong>{{session()->get('file_success')}}</strong> 
							</div>
						@endif
						
					 	<h3>{{$member->firstname}} {{$member->middle_name}} {{$member->lastname}}</h3>
					 	<p><strong>Rank: </strong>{{$member->rank}}</p>
					 	<p><strong>AFSPN: </strong>@if($member->afspn){{$member->afspn}}@else - @endif</p>
					 	<p><strong>Unit Assignment: </strong>{{$member->unit_assignment}}</p>
					 	<p><strong>Designation: </strong>{{$member->designation}}</p>
					 	<p><strong>Status: </strong>@if($member->status ==1)Active @elseif($member->status==2)Resigned @else Retired @endif </p>
					 	<div class="button-container">
					 		@if(Auth::user()->usertype_id!=3)
						 		<a class="btn btn-primary" href='{{url("members/$member->id/edit")}}' role="button">Edit</a>
					 			<a class="btn btn-danger" href="#delete-modal" data-toggle="modal" data-form-url='{{url("members/$member->id")}}'>Delete</a>
				 			@endif
					 	</div>
				 		

					 	<hr>
					 	<h4>Summary of Information</h4>
							@if(!isset($member->soi))
								@if(Auth::user()->usertype_id!=3)
									 <form class="dropzone" action='{{url("/members/$member->id/soi")}}' method="POST" id="soi_dropzone">
									 	<div class="dz-message" data-dz-message><span>No file uploaded yet. Click or drag files here for upload.</span></div>
									 </form>
								 	 <button type="submit" id="soi-save" class="soi-save btn btn-primary"> Save</button>
								@else
									<p>Sorry. You cannot upload any files.</p>
								@endif
							@else
								<div class="doc-container">
									<a href="{{url('/members/')}}/{{$member->id}}/soi/{{$member->soi->id}}">{{$member->soi->filename}}</a>
									@if(Auth::user()->usertype_id!=3)
										<a class="pull-right icon" href="#delete-modal" data-toggle="modal" data-form-url="{{url('/members/')}}/{{$member->id}}/soi/{{$member->soi->id}}"><i class="fa fa-times"></i></a>

										<a class="pull-right icon" data-modal-title="Edit Summary of Information" data-form-url="{{url('/members/')}}/{{$member->id}}/soi/{{$member->soi->id}}" data-filename="soi-{{$member->lastname}}-{{$member->id}}" data-toggle="modal" href="#edit-modal"><i class="fa fa-pencil-square-o"></i></a>
									@endif
								</div>	
							@endif
						<hr>
					 	<h4>NSO Birth Certificate</h4>
							@if(!isset($member->nso))
								@if(Auth::user()->usertype_id!=3)
								 <form class="dropzone" action='{{url("/members/$member->id/nso")}}' method="POST" id="nso_dropzone">
								 	<div class="dz-message" data-dz-message><span>No file uploaded yet. Click here for upload.</span></div>
								 </form>
							 	 <button type="submit" id="nso-save" class="soi-save btn btn-primary"> Save</button>
							 	 @else
									<p>Sorry. You cannot upload any files.</p>
								@endif
								 
							@else
								<div class="doc-container">
									<a href="{{url('/members/')}}/{{$member->id}}/nso/{{$member->nso->id}}">{{$member->nso->filename}}</a>
									@if(Auth::user()->usertype_id!=3)
										<a class="pull-right icon" href="#delete-modal" data-toggle="modal" data-form-url="{{url('/members/')}}/{{$member->id}}/nso/{{$member->nso->id}}"><i class="fa fa-times"></i></a>

										<a class="pull-right icon" data-modal-title="Edit NSO Birth Certificate" data-form-url="{{url('/members/')}}/{{$member->id}}/nso/{{$member->nso->id}}" data-filename="nso-{{$member->lastname}}-{{$member->id}}" data-toggle="modal" href="#edit-modal"><i class="fa fa-pencil-square-o"></i></a>
									@endif
								</div>	
								
							@endif
					 	<hr>
					 	<hr>
					 	<h4>AFP Appraisal Reports</h4>
							@if(count($member->appr_reports) == 0)
								@if(Auth::user()->usertype_id!=3)
								 <form class="dropzone" action='{{url("/members/$member->id/appr_reports")}}' method="POST" id="appr_reports_dropzone">
								 	<div class="dz-message" data-dz-message><span>No file uploaded yet. Click here for upload.</span></div>
								 </form>
							 	 <button type="submit" id="appr-reports-save" class="soi-save btn btn-primary"> Save</button>
							 	@else
									<p>Sorry. You cannot upload any files.</p>
								@endif
							@else

								@foreach($member->appr_reports as $appr_report)
								<div class="doc-container">
									<a href="{{url('/members/')}}/{{$member->id}}/appr_reports/{{$appr_report->id}}">{{$appr_report->filename}}</a>

									@if(Auth::user()->usertype_id!=3)
										<a class="pull-right icon" href="#delete-modal" data-toggle="modal" data-form-url="{{url('/members/')}}/{{$member->id}}/appr_reports/{{$appr_report->id}}"><i class="fa fa-times"></i></a>

										<a class="pull-right icon" data-modal-title="Edit Appraisal Report Document" data-form-url="{{url('/members/')}}/{{$member->id}}/appr_reports/{{$appr_report->id}}" data-filename="appr_report-{{$member->lastname}}-{{$member->id}}" data-toggle="modal" href="#edit-modal"><i class="fa fa-pencil-square-o"></i></a>
									@endif
								</div>	
								@endforeach
								@if(Auth::user()->usertype_id!=3)
									<form class="dropzone" action='{{url("/members/$member->id/appr_reports")}}' method="POST" id="appr_reports_dropzone">
									 	<div class="dz-message" data-dz-message><span>Click here to add files.</span></div>
									 </form>
								 	 <button type="submit" id="appr-reports-save" class="soi-save btn btn-primary"> Save</button>
							 	 @endif
							@endif
					 	<hr>
					 	<hr>
					 	<h4>CGSC Eligbility</h4>
							@if(!isset($member->cgsc))
								@if(Auth::user()->usertype_id!=3)
								 <form class="dropzone" action='{{url("/members/$member->id/cgsc")}}' method="POST" id="cgsc_dropzone">
								 	<div class="dz-message" data-dz-message><span>No file uploaded yet. Click here for upload.</span></div>
								 </form>
							 	 <button type="submit" id="cgsc-save" class="soi-save btn btn-primary"> Save</button>
							 	 @else
							 	 <p>Sorry! You cannot upload any files</p>
								 @endif
							@else
								<div class="doc-container">
									<a href="{{url('/members/')}}/{{$member->id}}/cgsc/{{$member->cgsc->id}}">{{$member->cgsc->filename}}</a>

									@if(Auth::user()->usertype_id!=3)
									<a class="pull-right icon" href="#delete-modal" data-toggle="modal" data-form-url="{{url('/members/')}}/{{$member->id}}/cgsc/{{$member->cgsc->id}}"><i class="fa fa-times"></i></a>

									<a class="pull-right icon" data-modal-title="Edit CGSC Eligbility Document" data-form-url="{{url('/members/')}}/{{$member->id}}/cgsc/{{$member->cgsc->id}}" data-filename="cgsc-{{$member->lastname}}-{{$member->id}}" data-toggle="modal" href="#edit-modal"><i class="fa fa-pencil-square-o"></i></a>
									@endif
								</div>	
							@endif
					 	<hr>
					 	<h4>Transcript of Records</h4>
							@if(count($member->tors) == 0)
								@if(Auth::user()->usertype_id!=3)
									 <form class="dropzone" action='{{url("/members/$member->id/tors")}}' method="POST" id="tors_dropzone">
									 	<div class="dz-message" data-dz-message><span>No file uploaded yet. Click here for upload.</span></div>
									 </form>
								 	 <button type="submit" id="tors-save" class="soi-save btn btn-primary"> Save</button>
								@else
									<p>Sorry! You cannot uplaod any files.</p>
								@endif
							@else
								@foreach($member->tors as $tors)
									<div class="doc-container">
										<a href="{{url('/members/')}}/{{$member->id}}/tors/{{$tors->id}}">{{$tors->filename}}</a>

										@if(!Auth::user()->usertype_id == 3)
											<a class="pull-right icon" href="#delete-modal" data-toggle="modal" data-form-url="{{url('/members/')}}/{{$member->id}}/tors/{{$tors->id}}"><i class="fa fa-times"></i></a>

											<a class="pull-right icon" data-modal-title="Edit Transcript of Record" data-form-url="{{url('/members/')}}/{{$member->id}}/tors/{{$tors->id}}" data-filename="tor-{{$member->lastname}}-{{$member->id}}" data-toggle="modal" href="#edit-modal"><i class="fa fa-pencil-square-o"></i></a>
										@endif

										
									</div>	
								@endforeach
								@if(!Auth::user()->usertype_id == 3)
									<form class="dropzone" action='{{url("/members/$member->id/tors")}}' method="POST" id="tors_dropzone">
									 	<div class="dz-message" data-dz-message><span>Click here to add files.</span></div>
									 </form>
								 	 <button type="submit" id="tors-save" class="soi-save btn btn-primary"> Save</button>
								@endif
								
							@endif
					 	<hr>
					 	<hr>
					 	<h4>Orders</h4>
					 		@if(count($member->orders)==0)
								@if(Auth::user()->usertype_id != 3)
									<a href="{{url('/orders/create')}}">
						 			<p class="text-center">There are no files uploaded yet </p>
									<p class="text-center">Click here to add an order</p>
									</a>
								@else
									<p>Sorry! You cannot upload any files.</p>
								@endif
					 			
							@else
								@foreach($member->orders as $order)
									<div class="doc-container">
									<a href='{{url("orders/$order->id")}}' data-toggle="tooltip" title="{{$order->description}}">{{$order->description}}</a>

									<a class="pull-right icon" href='{{url("orders/$order->id/download")}}'><i class="fa fa-download"></i></a>
								</div>
								@endforeach
					 		@endif
					 	<hr>
				 	</div>
				 </div>
			</div>	
		</div>
	</div>
	
	<!--edit SOI Modal-->
	<div class="modal fade" id="edit-modal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
					<div class="dropzone" id="edit-dropzone">
						<div class="dz-message" data-dz-message><span>Click here to upload file.</span></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="soi-edit-save">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" id="delete-modal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Confirm Delete</h4>
				</div>
				<div class="modal-body">
					<p style="font-size: 18px">You are about to delete a record. This procedure is irreversible.</p>
					<p style="font-size: 18px">Do you want to proceed?</p>
					<form id="delete-form" method="POST" action="#">
						{{csrf_field()}}
						{{method_field('DELETE')}}
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<input type="submit" form="delete-form" class="btn btn-danger" value="Delete"></input>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

@endsection

@section('scripts')
	<script src="/js/dropzone.js"></script>
	<script type="text/javascript">
		var editDropzone;
		Dropzone.options.soiDropzone = {
			autoProcessQueue: false,
			acceptedFiles: '.pdf, .jpg, .jpeg, .doc, .docx ',
			paramName: 'soi',
			maxFiles: 1,
			addRemoveLinks: true,
			headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },

			 init: function() {
			 	var dropzoneForm = this;
			    this.on("success", function(file, response) { 
			    	window.location.reload();
			    });

			    this.on("sending", function(file, xhr, formData){
	              formData.append('fileName', 'soi-'+"{{$member->lastname}}-{{$member->id}}");
	            });

             	this.on("addedfile", function() {
			      if (this.files[1]!=null){
			        this.removeFile(this.files[0]);
			      }
			    });

			    $('#soi-save').on("click", function(e) {
	              if(dropzoneForm.getQueuedFiles().length > 0){
	                dropzoneForm.processQueue();
	              }
	            });

			    this.on("error", function(file, response){
			    	$(file.previewElement).find('.dz-error-message').text(response).show().css({"opacity": 70, "top": "70px"});
            		$(file.previewElement).find('.dz-remove').text("Remove File");
			    });
			}
		}

		Dropzone.options.nsoDropzone = {
			autoProcessQueue: false,
			acceptedFiles: '.pdf, .jpg, .jpeg, .doc, .docx ',
			paramName: 'nso',
			maxFiles: 1,
			addRemoveLinks: true,
			headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },

			 init: function() {
			 	var dropzoneForm = this;
			    this.on("success", function(file, response) { 
			    	window.location.reload();
			    });

			    this.on("sending", function(file, xhr, formData){
	              formData.append('fileName', 'nso-'+"{{$member->lastname}}-{{$member->id}}");
	            });

             	this.on("addedfile", function() {
			      if (this.files[1]!=null){
			        this.removeFile(this.files[0]);
			    }

			    $('#nso-save').on("click", function(e) {
	              if(dropzoneForm.getQueuedFiles().length > 0){
	                dropzoneForm.processQueue();
	              }
	            });

			    this.on("error", function(file, response){
			    	$(file.previewElement).find('.dz-error-message').text(response).show().css({"opacity": 70, "top": "70px"});
            		$(file.previewElement).find('.dz-remove').text("Remove File");
			    });
		     });
			}
		}

		Dropzone.options.cgscDropzone = {
			autoProcessQueue: false,
			acceptedFiles: '.pdf, .jpg, .jpeg, .doc, .docx ',
			paramName: 'cgsc',
			maxFiles: 1,
			addRemoveLinks: true,
			headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },

			 init: function() {
			 	var dropzoneForm = this;
			    this.on("success", function(file, response) { 
			    	window.location.reload();
			    });

			    this.on("sending", function(file, xhr, formData){
	              formData.append('fileName', 'cgsc-'+"{{$member->lastname}}-{{$member->id}}");
	            });

             	this.on("addedfile", function() {
			      if (this.files[1]!=null){
			        this.removeFile(this.files[0]);
			    }

			    $('#cgsc-save').on("click", function(e) {
	              if(dropzoneForm.getQueuedFiles().length > 0){
	                dropzoneForm.processQueue();
	              }
	            });

			    this.on("error", function(file, response){
			    	$(file.previewElement).find('.dz-error-message').text(response).show().css({"opacity": 70, "top": "70px"});
            		$(file.previewElement).find('.dz-remove').text("Remove File");
			    });
		     });
			}
		}

		Dropzone.options.apprReportsDropzone = {
			autoProcessQueue: false,
			acceptedFiles: '.pdf, .jpg, .jpeg, .doc, .docx ',
			paramName: 'appr_reports',
			maxFiles: 100,
			addRemoveLinks: true,
			parallelUploads: 1000,
			uploadMultiple: true,
			headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },

			 init: function() {
			 	var dropzoneForm = this;
			    this.on("success", function(file, response) { 
			    	window.location.reload();
			    });

			    this.on("sending", function(file, xhr, formData){
	              formData.append('fileName', 'appr_report-'+"{{$member->lastname}}-{{$member->id}}");
	            });

			    $('#appr-reports-save').on("click", function(e) {
	              if(dropzoneForm.getQueuedFiles().length > 0){
	                dropzoneForm.processQueue();
	              }
	            });

			    this.on("error", function(file, response){
			    	$(file.previewElement).find('.dz-error-message').text(response).show().css({"opacity": 70, "top": "70px"});
            		$(file.previewElement).find('.dz-remove').text("Remove File");
			    });
		     }
		}

		Dropzone.options.torsDropzone = {
			autoProcessQueue: false,
			acceptedFiles: '.pdf, .jpg, .jpeg, .doc, .docx ',
			paramName: 'tors',
			maxFiles: 100,
			addRemoveLinks: true,
			parallelUploads: 1000,
			uploadMultiple: true,
			headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },

			 init: function() {
			 	var dropzoneForm = this;
			    this.on("success", function(file, response) { 
			    	window.location.reload();
			    });

			    this.on("sending", function(file, xhr, formData){
	              formData.append('fileName', 'tor-'+"{{$member->lastname}}-{{$member->id}}");
	            });

			    $('#tors-save').on("click", function(e) {
	              if(dropzoneForm.getQueuedFiles().length > 0){
	                dropzoneForm.processQueue();
	              }
	            });

			    this.on("error", function(file, response){
			    	$(file.previewElement).find('.dz-error-message').text(response).show().css({"opacity": 70, "top": "70px"});
            		$(file.previewElement).find('.dz-remove').text("Remove File");
			    });
		     }
		}

		Dropzone.options.editDropzone = false;


		$('#edit-modal').on('show.bs.modal', function (event) {
		  	var trigger = $(event.relatedTarget) 
		  	var modalTitle = trigger.data('modal-title')
		  	var formUrl = trigger.data('form-url')
		  	var fileName = trigger.data('filename')

		 	var modal = $(this)

		 	editDropzone = $('#edit-modal #edit-dropzone').dropzone({
		 		url: formUrl,
		 		autoProcessQueue: false,
				acceptedFiles: '.pdf, .jpg, .jpeg, .doc, .docx ',
				paramName: 'file',
				maxFiles: 1,
				addRemoveLinks: true,
				headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        },

		        init: function() {
				 	var dropzoneForm = this;
				    this.on("success", function(file, response) { 
				    	window.location.reload();
				    });

				    this.on("sending", function(file, xhr, formData){
		              formData.append('fileName', fileName);
		            });

	             	this.on("addedfile", function() {
				      if (this.files[1]!=null){
				        this.removeFile(this.files[0]);
				       }
				    });

				    $('#soi-edit-save').on("click", function(e) {
		              if(dropzoneForm.getQueuedFiles().length > 0){
		                dropzoneForm.processQueue();
		              }
		            });

				    $('#edit-modal').on('hide.bs.modal', function (e) {
						dropzoneForm.destroy();
					});

		 		}

			});

			modal.find('.modal-title').text(modalTitle)
		});

		$('#delete-modal').on('show.bs.modal', function (event){
			var trigger = $(event.relatedTarget);
			var formUrl = trigger.data('form-url');

			var modal = $(this);

			modal.find('#delete-form').attr('action', formUrl);

		});

	</script>
@endsection