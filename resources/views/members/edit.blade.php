@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Edit Member</strong></div>

                    <div class="panel-body">
                        <h3 style="margin-bottom: 40px;" class="text-center">Edit Member Information</h3>
                        <form action='{{url("members/$member->id")}}' method="POST">
                            {{method_field('PUT')}}
                        	{{csrf_field()}}
                        	<div class="form-group row {{ $errors->has('firstname') ? ' has-error' : '' }}">
                        		<label for="firstname" class="col-sm-2 col-md-offset-1 form-control-label">Firstname</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname" value="{{$errors->has('firstname') ? old('firstname'):  $member->firstname }}">

                        			@if ($errors->has('firstname'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('firstname') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                            <div class=" form-group row {{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                <label for="middle_name" class="col-sm-2 col-md-offset-1  form-control-label">Middle Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="middle_name" name="middle_name" placeholder="middle_name" value="{{$errors->has('middle_name') ? old('middle_name'):  $member->middle_name }}">
                                    @if ($errors->has('middle_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('middle_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        	<div class=" form-group row {{ $errors->has('lastname') ? ' has-error' : '' }}">
                        		<label for="lastname" class="col-sm-2 col-md-offset-1  form-control-label">Lastname</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Lastname" value="{{$errors->has('lastname') ? old('lastname'):  $member->lastname }}">
                        			@if ($errors->has('lastname'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('lastname') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class=" form-group row {{ $errors->has('rank') ? ' has-error' : '' }}">
                                    <label for="rank" class="col-sm-2 col-md-offset-1 form-control-label">Rank</label>
                                    <div class="col-sm-7">
                                          <select name = "rank" id="rank">
                                                <option value="" {{ $errors->has('rank') ? old('rank')=="" ? 'selected': "" : $member->rank == "" ? "selected" : ""}}>Rank</option>
                                                <option value="P2LT" {{ $errors->has('rank') ? old('rank')=="P2LT" ? 'selected': "" : $member->rank == "P2LT" ? "selected" : ""}}>P2LT</option>
                                                <option value="CPT" {{ $errors->has('rank') ? old('rank')=="CPT" ? 'selected': "" : $member->rank == "CPT" ? "selected" : ""}}>CPT</option>
                                                <option value="MAJ" {{ $errors->has('rank') ? old('rank')=="MAJ" ? 'selected': "" : $member->rank == "MAJ" ? "selected" : ""}}>MAJ</option>
                                                <option value="LTC" {{ $errors->has('rank') ? old('rank')=="LTC" ? 'selected': "" : $member->rank == "LTC" ? "selected" : ""}}>LTC</option>
                                                <option value="COL" {{ $errors->has('rank') ? old('rank')=="COL" ? 'selected': "" : $member->rank == "COL" ? "selected" : ""}}>COL</option>
                                                <option value="BGEN" {{ $errors->has('rank') ? old('rank')=="BGEN" ? 'selected': "" : $member->rank == "BGEN" ? "selected" : ""}}>BGEN</option>
                                          </select>
                                    </div>
                              </div>

                        	<div class=" form-group row {{ $errors->has('afspn') ? ' has-error' : '' }}">
                        		<label for="AFSPN" class="col-sm-2 col-md-offset-1 form-control-label">AFSPN</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="afspn" name="afspn" placeholder="AFSPN" value="{{$errors->has('afspn') ? old('afspn'):  $member->afspn }}"> 

                        			@if ($errors->has('afspn'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('afspn') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class=" form-group row {{ $errors->has('unit_assignment') ? ' has-error' : '' }}">
                        		<label for="unit_assignment" class="col-sm-2 col-md-offset-1 form-control-label">Unit Assignment</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="unit_assignment" name="unit_assignment" placeholder="Unit Assignment" value="{{$errors->has('unit_assignment') ? old('unit_assignment'):  $member->unit_assignment }}">

                        			@if ($errors->has('unit_assignment'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('unit_assignment') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                        	<div class=" form-group row {{ $errors->has('designation') ? ' has-error' : '' }}">
                        		<label for="unit_assignment" class="col-sm-2 col-md-offset-1 form-control-label">Designation</label>
                        		<div class="col-sm-7">
                        			<input type="text" class="form-control" id="designation" name="designation" placeholder="Designation" value="{{$errors->has('designation') ? old('designation'):  $member->designation }}">

                        			@if ($errors->has('designation'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('designation') }}</strong>
	                                    </span>
	                                @endif
                        		</div>
                        	</div>

                            <div class=" form-group row {{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="unit_assignment" class="col-sm-2 col-md-offset-1 form-control-label">Status</label>
                                <div class="col-sm-7">
                                    <select name = "status" id="status">
                                        <option value=1 {{ $errors->has('status') ? old('status')==1 ? 'selected': "" : $member->status == 1 ? "selected" : ""}}>Active</option>
                                        <option value=2 {{ $errors->has('status') ? old('status')==2 ? 'selected': "" : $member->status == 2 ? "selected" : ""}}>Resigned</option>
                                        <option value=3 {{ $errors->has('status') ? old('status')==3 ? 'selected': "" : $member->status == 3 ? "selected" : ""}}>Retired</option>
                                  </select>

                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        	
                        	<div class="form-group row">
                        		<div class="col-sm-10 col-sm-offset-3">
                        			<button type="submit" class="btn btn-secondary">Save</button>
                        		</div>
                        	</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	</div>
@endsection

@section('scripts')
    <script>
        $('#rank').selectize({
            hightlight: false,
            placeholder: 'Rank',
        });

        $('#status').selectize({
            hightlight: false,
            placeholder: 'Status',
        });
    </script>
@endsection