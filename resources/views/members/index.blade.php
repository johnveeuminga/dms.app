@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>COP Profile Management</strong></div>
                    
                    <div class="panel-body">
                    <p></p>
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <strong>{{session()->get('success')}}</strong> 
                            </div>
                        @endif
                       
                        <table class="table" id="members-table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Rank</th>
                                    <th>AFSPN</th>
                                    <th>Unit Assignment</th>
                                    <th>Designation</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="members-tbody">
                                @foreach($members as $member)
                                    <tr>
                                        <th>{{$member->firstname}} {{$member->middle_initial}} {{$member->lastname}}</th>
                                        <td>{{$member->rank}}</td>
                                        <td>
                                        	@if($member->afspn)
                                        		{{$member->afspn}}
                                        	@else
                                        		-
                                        	@endif
                                        </td>
                                        <td>{{$member->unit_assignment}}</td>
                                        <td>{{$member->designation}}</td>
                                        <td><a class="btn btn-primary" href='{{url("members/$member->id")}}' role="button">View</a></td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                        @if(Auth::user()->usertype_id!=3)
                       	<a href="{{url('members/create')}}" class="btn btn-primary">Add</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
	</div>
@endsection
@section('scripts')
    <script>
        // $('#search').selectize({
        //     highlight:false,
        //     placeholder: "Enter a member's name",
        //     allowEmptyOptions: true,
        //     onChange : function(value){
        //         $('#members-tbody').html('<h2 class="text-center">Please wait....</h2>').show();
        //         var url;
        //         if(value!=""){
        //             url= "{{url("/members/")}}"+"/"+value+"/get";

        //         }else{
        //             url= "{{url("/members/")}}"+"/all"+"/getAll";
        //         }

        //         $.ajax({
        //                 url: url,
        //                 method: 'GET',
        //                 header: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        //                 dataType: 'html',
        //                 success: function(response){
        //                     $('#members-table tbody').html(response).show();
        //                 }
        //             })
        //     }
        // });

        $('#members-table').DataTable();
    </script>
@endsection