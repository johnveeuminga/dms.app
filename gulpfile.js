const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */
elixir(function(mix) {
    mix.browserSync({
        proxy: 'dms.app'
    });
});

elixir((mix) => {
    mix.sass('app.scss')
       .webpack('app.js')
       .copy('node_modules/dropzone/dist/dropzone.js', 'public/js/dropzone.js')
       .less('./node_modules/selectize/dist/less/selectize.less')
       .copy('node_modules/selectize/dist/js/standalone/selectize.js', 'public/js/selectize.js')
});
