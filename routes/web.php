<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::resource('/users', 'UserController');
Route::get('/users/{user}/get', 'UserController@getUser');
Route::get('/users/all/getAll', 'UserController@getAllUsers');

Route::resource('/members', 'MemberController');
Route::get('/members/{member}/get', 'MemberController@getMember');
Route::get('/members/all/getAll', 'MemberController@getAllMembers');

Route::resource('members.soi', 'MemberSoiController', ['except' => ['update']]);
Route::POST('/members/{member}/soi/{soi}', 'MemberSoiController@update');

Route::resource('members.nso', 'MemberNsoController',  ['except' => ['update']]);
Route::POST('/members/{member}/nso/{nso}', 'MemberNsoController@update');

Route::resource('members.cgsc', 'MemberCgscController', ['except' => ['update']]);
Route::POST('/members/{member}/cgsc/{cgsc}', 'MemberCgscController@update');

Route::resource('members.appr_reports', 'MemberApprReportsController', ['except' => ['update']]);
Route::POST('/members/{member}/appr_reports/{appr_report}', 'MemberApprReportsController@update');

Route::resource('members.tors', 'MemberTorController', ['except' => ['update']]);
Route::POST('/members/{member}/tors/{tor}', 'MemberTorController@update');

Route::resource('/orders', 'OrderController');
Route::get('/orders/{order}/download', 'OrderController@download');
Route::post('/orders/{order}', 'OrderController@update');

Route::get('/profile/edit', 'ProfileController@edit');
Route::put('/profile', 'ProfileController@update');
Route::get('/profile/changepass', 'ProfileController@changepass');
Route::put('/profile/changepass', 'ProfileController@passchange');

Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
