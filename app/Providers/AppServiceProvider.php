<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Observers\MemberObserver;

use App\Member;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Member::observe(MemberObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
