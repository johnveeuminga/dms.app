<?php

namespace App\Observers;

use App\Member;

use Illuminate\Support\Facades\Storage;

class MemberObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(Member $member)
    {
        //
    }

    /**
     * Listen to the Member deleting event.
     *
     * @param  Member  $Member
     * @return void
     */
    public function deleting(Member $member)
    {
        if(($member->soi)){
            if(Storage::exists($member->soi->path)){
               Storage::delete($member->soi->path);
            }
        }

        if(($member->nso)){
            if(Storage::exists($member->nso->path)){
               Storage::delete($member->nso->path);
            }
        }

        if(($member->cgsc)){
            if(Storage::exists($member->cgsc->path)){
               Storage::delete($member->cgsc->path);
            }
        }

        if($member->appr_reports){
            foreach ($member->appr_reports as $key => $appr_report) {
                if(Storage::exists($appr_report->path)){
                   Storage::delete($appr_report->path);
                }
            }
        }

        if($member->tors){
            foreach ($member->tors as $key => $tor) {
                if(Storage::exists($tor->path)){
                   Storage::delete($tor->path);
                }
            }
        }

        if($member->orders){
            foreach ($member->orders as $key => $order) {
                if(Storage::exists($order->path)){
                   Storage::delete($order->path);
                }
            }
        }

    }
}