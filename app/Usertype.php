<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usertype extends Model
{
    protected $fillable = ['usertype_desc', 'usertype_code'];

    public function users(){
    	return $this->hasMany('users');
    }
}
