<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MemberCgsc;

use Illuminate\Support\Facades\Storage;

class MemberCgscController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $memberId)
    {
        //
        $filename = $request->fileName.'-'.time().'.'.$request->file('cgsc')->getClientOriginalExtension();
        $cgsc = new MemberCgsc;

        $path = $request->file('cgsc')->storeAs('cgscs', $filename);

        $cgsc->path = $path;
        $cgsc->fileName = $filename;
        $cgsc->member_id = $memberId;

        $cgsc->save();

        $request->session()->flash('file_success', 'File successfuly added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($memberId, $fileId)
    {
        $file = MemberCgsc::find($fileId);
        return response()->download(storage_path('app/'.$file->path));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $memberId, $cgscId)
    {
        $cgsc = MemberCgsc::find($cgscId);

        if(Storage::exists($cgsc->path)){
            Storage::delete($cgsc->path);
        }

        $fileName = $request->fileName.'-'.time().'.'.$request->file('file')->getClientOriginalExtension();

        $path = $request->file('file')->storeAs('cgscs', $fileName);

        $cgsc->path = $path;
        $cgsc->fileName = $fileName;

        $cgsc->save();

        $request->session()->flash('file_success', 'File was successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($memberId, $cgscId)
    {
        $cgsc = MemberCgsc::find($cgscId);

        if(Storage::exists($cgsc->path)){
            Storage::delete($cgsc->path);
        }

        $cgsc->delete();

        return redirect('/members/'.$memberId)->with('success', 'File successfuly deleted.');
    }
}
