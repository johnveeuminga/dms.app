<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MemberSoi;

use App\Member;

use Illuminate\Support\Facades\Storage;

class MemberSoiController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $memberId)
    {
        $fileName = $request->fileName.'-'.time().'.'.$request->file('soi')->getClientOriginalExtension();
        $soi = new MemberSoi;

        $path = $request->file('soi')->storeAs('sois', $fileName);

        $soi->path = $path;
        $soi->fileName = $fileName;
        $soi->member_id = $memberId;

        $soi->save();

        $request->session()->flash('file_success', 'File successfuly added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($memberId, $fileId)
    {
        $file = MemberSoi::find($fileId);
        return response()->file(storage_path('app/'.$file->path));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $memberId, $soiId)
    {
        $soi = MemberSoi::find($soiId);

        if(Storage::exists($soi->path)){
            Storage::delete($soi->path);
        }

        $fileName = $request->fileName.'-'.time().'.'.$request->file('file')->getClientOriginalExtension();

        $path = $request->file('file')->storeAs('sois', $fileName);

        $soi->path = $path;
        $soi->fileName = $fileName;

        $soi->save();

        $request->session()->flash('file_success', 'File was successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($memberId, $soiId)
    {
        $soi = MemberSoi::find($soiId);

        if(Storage::exists($soi->path)){
            Storage::delete($soi->path);
        }

        $soi->delete();

        return redirect('/members/'.$memberId)->with('success', 'File successfuly deleted.');
    }
}
