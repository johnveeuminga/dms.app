<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MemberTor;

use Illuminate\Support\Facades\Storage;

class MemberTorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $memberId)
    {
    	foreach($request->file('tors') as $tor)
    	{
            $path = $tor->store('tors');

	        $memberTor = new MemberTor;

	       $memberTor->path = $path;
	       $memberTor->member_id = $memberId;
	       $memberTor->save();

           $filename = $request->fileName.'-'.$memberTor->id.'-'.time().'.'.$tor->getClientOriginalExtension();
           Storage::move($path, 'tors/'.$filename);
           $newPath = ('/tors/'.$filename);
           $memberTor->path = $newPath;
           $memberTor->fileName = $filename;
           $memberTor->save();
    	}


       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($memberId, $fileId)
    {
        $file = MemberTor::find($fileId);
        return response()->download(storage_path('app/'.$file->path));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $memberId, $torId)
    {
        $tor = MemberTor::find($torId);

        if(Storage::exists($tor->path)){
            Storage::delete($tor->path);
        }

        $path = $request->file('file')->store('tors');

        $tor->path = $path;
        $tor->save();

        $fileName = $request->fileName.'-'.$torId.'-'.time().'.'.$request->file('file')->getClientOriginalExtension();

        Storage::move($path, 'tors/'.$fileName);
        $newPath = ('/tors/'.$fileName);
        $tor->path = $newPath;
        $tor->fileName = $fileName;
        $tor->save();

        $request->session()->flash('file_success', 'File was successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($memberId, $torId)
    {
        $tor = MemberTor::find($torId);

        if(Storage::exists($tor->path)){
            Storage::delete($tor->path);
        }

        $tor->delete();

        return redirect('/members/'.$memberId)->with('success', 'File successfuly deleted.');
    }
}
