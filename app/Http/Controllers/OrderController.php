<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Http\Requests\AddOrderRequest;

use App\Http\Requests\EditOrderRequest;

use App\Order;

use App\Member;

use App\OrderType;

use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('orders.index')->with('orders', $orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $members = Member::all();
        $ordertypes = OrderType::all();
        return view('orders.create')->with('members', $members)->with('ordertypes', $ordertypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddOrderRequest $request)
    {
        $dt=Carbon::now()->toDateString();
        if($request->ordertype == 1)
        {
            $fileName = 'APPOR-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
        }elseif($request->ordertype == 2){
            $fileName = 'RESOR-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
        }elseif($request->ordertype==3){
            $fileName = 'RETOR-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
        }elseif($request->ordertype==4){
            $fileName = 'ORDTODUT-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
        }elseif($request->ordertype==5){
            $fileName = 'PROMOR-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
        }elseif($request->ordertype==6){
            $fileName = 'EXTAPP-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
        }else{
            $fileName = 'OTHERS-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
        }


        $order = new Order;

        $path = $request->file('orderFile')->storeAs('orders', $fileName);

        $order->path = $path;
        $order->fileName = $fileName;
        $order->description  = $request->comment;
        $order->ordertype_id = $request->ordertype;

        $order->save();

        foreach($request->member as $member){
            $order->members()->attach($member);
        }

        return redirect('/orders')->with('success', 'Order successfuly created.'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        return view('orders.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $ordertypes = OrderType::all();
        $members = Member::all();

        return view('orders.edit')->with('order', $order)->with('ordertypes', $ordertypes)->with('members', $members);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditOrderRequest $request, $id)
    {
        $order = Order::find($id);

        $dt=Carbon::now()->toDateString();


        if($request->hasFile('orderFile')){
            if(Storage::exists($order->path)){
                Storage::delete($order->path);
            }

            if($request->hasFile('orderFile')){
                if($request->ordertype == 1)
                {
                    $fileName = 'SO-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
                }elseif($request->ordertype == 2){
                    $fileName = 'LO-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
                }elseif($request->ordertype==3){
                    $fileName = 'GO-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
                }else{
                    $fileName = 'OTHERS-'.$dt.'.'.$request->file('orderFile')->getClientOriginalExtension();
                }

                $path = $request->file('orderFile')->storeAs('orders', $fileName);

                $order->path = $path;
                $order->filename = $fileName;
            }

        }
        
        $order->ordertype_id = $request->ordertype;
        $order->description = $request->comment;
        $order->members()->sync($request->member);
  
        $order->save();

        return redirect('/orders/'.$order->id)->with('success', 'Order successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);

        if(Storage::exists($order->path)){
            Storage::delete($order->path);
        }

        $order->delete();

        return redirect('/orders/')->with('success', 'File successfuly deleted.');
    }

    public function download($id){
        $file = Order::find($id);

        return response()->download(storage_path('app/'.$file->path), $file->filename);
    }
}
