<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MemberNso;

use Illuminate\Support\Facades\Storage;

class MemberNsoController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $memberId)
    {
        //
        $fileName = $request->fileName.'-'.time().'.'.$request->file('nso')->getClientOriginalExtension();

        $nso = new MemberNso;

        $path = $request->file('nso')->storeAs('nsos', $fileName);

        $nso->path = $path;
        $nso->fileName = $fileName;
        $nso->member_id = $memberId;

        $nso->save();

        $request->session()->flash('file_success', 'File successfuly added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($memberId, $fileId)
    {
        $file = MemberNso::find($fileId);
        return response()->download(storage_path('app/'.$file->path));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $memberId, $nsoId)
    {
         $nso = MemberNso::find($nsoId);

        if(Storage::exists($nso->path)){
            Storage::delete($nso->path);
        }

        $fileName = $request->fileName.'-'.time().'.'.$request->file('file')->getClientOriginalExtension();

        $path = $request->file('file')->storeAs('nsos', $fileName);

        $nso->path = $path;
        $nso->fileName = $fileName;

        $nso->save();

        $request->session()->flash('file_success', 'File was successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($memberId, $nsoId)
    {
        $nso = MemberNso::find($nsoId);

        if(Storage::exists($nso->path)){
            Storage::delete($nso->path);
        }

        $nso->delete();

        return redirect('/members/'.$memberId)->with('success', 'File successfuly deleted.');
    }
}
