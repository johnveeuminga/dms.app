<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MemberApprReports;

use Illuminate\Support\Facades\Storage;

class MemberApprReportsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $memberId)
    {
    	foreach($request->file('appr_reports') as $appr_report)
    	{
            $path = $appr_report->store('appr_reports');

	        $apprReport = new MemberApprReports;

	        $apprReport->path = $path;
	        $apprReport->member_id = $memberId;
	        $apprReport->save();

            $filename = $request->fileName.'-'.$apprReport->id.'-'.time().'.'.$appr_report->getClientOriginalExtension();
            Storage::move($path, 'appr_reports/'.$filename);
            $newPath = ('/appr_reports/'.$filename);
            $apprReport->path = $newPath;
            $apprReport->fileName = $filename;
            $apprReport->save();


    	}


       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($memberId, $fileId)
    {
        $file = MemberApprReports::find($fileId);
        return response()->download(storage_path('app/'.$file->path));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $memberId, $apprReportId)
    {
        $apprReport = MemberApprReports::find($apprReportId);

        if(Storage::exists($apprReport->path)){
            Storage::delete($apprReport->path);
        }

        $path = $request->file('file')->store('appr_reports');

        $apprReport->path = $path;
        $apprReport->save();

        $fileName = $request->fileName.'-'.$apprReport->id.'-'.time().'.'.$request->file('file')->getClientOriginalExtension();

        Storage::move($path, 'appr_reports/'.$fileName);
        $newPath = ('/appr_reports/'.$fileName);
        $apprReport->path = $newPath;
        $apprReport->fileName = $fileName;
        $apprReport->save();

        $request->session()->flash('file_success', 'File was successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($memberId, $apprReportId)
    {
        $apprReport = MemberApprReports::find($apprReportId);

        if(Storage::exists($apprReport->path)){
            Storage::delete($apprReport->path);
        }

        $apprReport->delete();

        return redirect('/members/'.$memberId)->with('success', 'File successfuly deleted.');
    }
}
