<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Member;

use App\Http\Requests\MemberRequest;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $members = Member::with('soi','nso','appr_reports')->get();

        return view('members.index')->with('members', $members);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberRequest $request)
    {
        $member = Member::create($request->all());

        return redirect('/members/'.$member->id.'/')->with('member', $member);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::with('soi', 'nso', 'appr_reports', 'tors')->where('id', $id)->first();

        return view('members.show')->with('member', $member);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);

        return view('members.edit')->with('member', $member);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MemberRequest $request, $id)
    {
        $tempMember = Member::find($id);

        if(!$tempMember){
            return redirect(url('members'));
        }

        $member = $tempMember->update($request->all());

         return redirect('/members/'.$id.'/')->with('success', 'Member successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::find($id);

        $member->delete();

        return redirect('members')->with('success', 'Member successfully deleted.');
    }

    public function getMember($id){
        $member = Member::find($id);

        return view('helpers.members-table-helper')->with('member', $member);
    }

    public function getAllMembers(){
        $members = Member::all();

        return view('helpers.members-table-helper')->with('members', $members);
    }

}
