<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string',
            'middle_name' => 'string',
            'lastname' => 'required|string',
            'rank' => 'required|string',
            'unit_assignment' => 'required|string',
            'designation' => 'required|string',
            'status' => 'required'
        ];
    }
}
