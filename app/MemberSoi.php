<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberSoi extends Model
{
    protected $fillable = ['filename', 'path', 'member_id'];
    
    public function member(){
    	return $this->belongsTo('App\Member');
    }
}
