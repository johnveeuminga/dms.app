<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = ['path', 'filename', 'ordertype_id', 'member_id'];

	public function ordertype(){
		return $this->belongsTo('App\OrderType');
	}

	public function members(){
		return $this->belongsToMany('App\Member', 'order_member', 
      'member_id', 'order_id');
	}
}
