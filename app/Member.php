<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = ['firstname', 'middle_name', 'lastname', 'rank', 'afspn', 'unit_assignment', 'designation', 'status'];

    public function soi(){
        return $this->hasOne('App\MemberSoi');
    }


    public function nso(){
    	return $this->hasOne('App\MemberNso');
    }

    public function cgsc(){
    	return $this->hasOne('App\MemberCgsc');
    }

    public function appr_reports(){
        return $this->hasMany('App\MemberApprReports');
    }

    public function tors(){
        return $this->hasMany('App\MemberTor');
    }

    public function orders(){
        return $this->belongsToMany('App\Order', 'order_member', 
      'order_id', 'member_id');
    }

}
